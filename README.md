# Repositorio de configuraci�n

## Estructura del repositorio
Dentro del directorio _Arquitectura/EtapaXX_ se encuentran diferentes documentos. Los mismos estan separados en subdirectorios según su clasificación. Estos son:

- Diagramas C4, donde se explica en detalle el contexto, contenedores y componentes que son parte e interactuan en el sistema desarrollado.
- JSON (API Documentation.postman_collection y Production.postman_environment) que pueden ser utilizados en Postman para la prueba de cada enpoint de la aplicación.

**NOTA:** Una vez exportados estos archivos en _Postman_ y antes de ejecutar los diferentes endpoints, se debe seleccionar como _environment_ (en la parte superior derecha) la opción _Production_ y se deberá setear en la variable **PORT** el valor del puerto en el que este corriendo la aplicación (ingresando a _environments >> Production_).

## Cómo empezar?

- Clonar el repositorio
- Ejecutar en la terminal el comando `docker compose up`